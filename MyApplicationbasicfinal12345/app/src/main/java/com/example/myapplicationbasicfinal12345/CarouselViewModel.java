package com.example.myapplicationbasicfinal12345;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CarouselViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CarouselViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}