package com.example.myapplicationbasicfinal12345.entity;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class InvoiceViewModel extends ViewModel {

    private List<Invoice> invoices = new ArrayList();

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices.clear();
        this.invoices.addAll(invoices);
    }
}
