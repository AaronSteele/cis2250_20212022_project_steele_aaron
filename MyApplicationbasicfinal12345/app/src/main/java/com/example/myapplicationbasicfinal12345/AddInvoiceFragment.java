package com.example.myapplicationbasicfinal12345;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.fragment.NavHostFragment;

import com.example.myapplicationbasicfinal12345.databinding.FragmentAddInvoiceBinding;
import com.example.myapplicationbasicfinal12345.entity.Invoice;
import com.example.myapplicationbasicfinal12345.entity.InvoiceRepository;
import com.example.myapplicationbasicfinal12345.entity.InvoiceViewModel;
import com.example.myapplicationbasicfinal12345.util.ContentProviderUtil;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * AddInvoiceFragment
 * This Java class is associated with the add invoice fragment.  Note it works with the controls defined
 * on the res/layout/fragment_add_invoice layout file
 *
 * @author BJM/CIS2250
 * @since 20220129
 */

public class AddInvoiceFragment extends Fragment {

    public static final String KEY = "info.hccis.performancehall.INVOICE";
    private FragmentAddInvoiceBinding binding;
    private Invoice invoice;
    private InvoiceViewModel invoiceViewModel;
    private InvoiceRepository invoiceRepository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("AddInvoiceFragment Aaron", "onCreate triggered");
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        Log.d("AddInvoiceFragment Aaron", "onCreateView triggered");
        binding = FragmentAddInvoiceBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddInvoiceFragment BJM", "onViewCreated triggered");

        invoiceViewModel = new ViewModelProvider(getActivity()).get(InvoiceViewModel.class);

        //******************************************************************************
        // Content Providers
        // Check permission for user to use the calendar provider.
        //******************************************************************************

        final int callbackId = 42;
        ContentProviderUtil.checkPermission(getActivity(), callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);

        invoiceRepository = invoiceRepository.getInstance();
        //************************************************************************************
        // Add a listener on the button.
        //************************************************************************************

        binding.buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("AddInvoiceFragment Aaron", "Calculate was clicked");

                try {
                    //************************************************************************************
                    // Call the calculate method
                    //************************************************************************************

                    calculate();

                    //************************************************************************************
                    // I have made the TicketOrderBO implement Serializable.  This will allow us to
                    // serialize the object and then it can be passed in the bundle.  Note that the
                    // calculate method also sets the
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, invoice);

                    //************************************************************************************
                    // See the view model research component.  The ViewModel is associated with the Activity
                    // and the add and view fragments share the same activity.  Am using the view model
                    // to hold the list of TicketOrderBO objects.  This will allow both fragments to
                    // access the list.
                    //************************************************************************************

                    //******************************************************************************
                    // Handle the new ticket order
                    // Use the post service to add the new ticket to the web database.
                    //******************************************************************************

                    invoiceRepository.getInvoiceService().createInvoice(invoice).enqueue(new Callback<Invoice>() {
                        @Override
                        public void onResponse(Call<Invoice> call, Response<Invoice> r) {
                            Toast.makeText(getContext(), "Invoice is successfully added!", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Invoice> call, Throwable t) {
                            Toast.makeText(getContext(), "Error Adding Invoice: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });


                    //******************************************************************************
                    //Activity for Thursday.
                    //Call the post rest api web service to have the new ticket order added to the database.
                    //******************************************************************************

                    Snackbar.make(view, "Have added an invoice.  Want to add this new invoice to the database. " +
                            "How should we handle this?", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    //******************************************************************************
                    //Create the event without using Calendar intent
                    //******************************************************************************

                    long eventID = ContentProviderUtil.createEvent(getActivity(), invoice.toString());
                    Toast.makeText(getActivity(), "Calendar Event Created (" + eventID + ")", Toast.LENGTH_SHORT);
                    Log.d("Aaron Calendar", "Calendar Event Created (" + eventID + ")");

                    //******************************************************************************
                    // Send a broadcast.
                    //******************************************************************************

                    //Send a broadcast.
                    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(view.getContext());
                    Intent intent = new Intent();
                    intent.setAction("info.hccis.phall.order");
                    lbm.sendBroadcast(intent);
                    getActivity().sendBroadcast(intent);


                    //************************************************************************************
                    // Navigate to the view invoice fragment.  Note that the navigation is defined in
                    // the nav_graph.xml file.
                    //************************************************************************************

                    NavHostFragment.findNavController(AddInvoiceFragment.this)
                            .navigate(R.id.action_AddInvoiceFragment_to_ViewInvoicesFragment, bundle);
                } catch (Exception e) {
                    //************************************************************************************
                    // If there was an exception thrown in the calculate method, just put a message
                    // in the Logcat and leave the user on the add fragment.
                    //************************************************************************************
                    Log.d("AddInvoiceFragment BJM", "Error calculating: " + e.getMessage());
                }
            }
        });

    }

    public void calculate() throws Exception {

        Log.d("BJM-MainActivity", "ID =" + binding.editTextId.getText().toString());
        Log.d("BJM-MainActivity", "Tutor ID = " + binding.editTextTutorId.getText().toString());
        Log.d("BJM-MainActivity", "Start Date =" + binding.editTextId.getText().toString());
        Log.d("BJM-MainActivity", "End Date = " + binding.editTextTutorId.getText().toString());
        Log.d("BJM-MainActivity", "Add Invoice button was clicked.");

        int id = 0;
        try {
            id = Integer.parseInt(binding.editTextId.getText().toString());
        } catch (Exception e) {
            id = 0;
        }
        int tutorId = 0;
        try {
            tutorId = Integer.parseInt(binding.editTextTutorId.getText().toString());
        } catch (Exception e) {
            tutorId = 0;
        }
        String startDate = "";
        try {
            startDate = (binding.editTextTutorId.getText().toString());
        } catch (Exception e) {
            startDate = "";
        }
        String endDate = "";
        try {
            endDate = (binding.editTextTutorId.getText().toString());
        } catch (Exception e) {
            endDate = "";
        }

        invoice = new Invoice();
        invoice.setId(id);
        invoice.setTutorId(tutorId);
        invoice.setStartDate(startDate);
        invoice.setEndDate(endDate);

    }
}