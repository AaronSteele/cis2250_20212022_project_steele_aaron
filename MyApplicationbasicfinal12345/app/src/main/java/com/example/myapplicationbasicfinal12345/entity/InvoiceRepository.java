package com.example.myapplicationbasicfinal12345.entity;

/*
 * @Author: BJM (Modified from Mariana Alkabalan (Flower Shop App)
 * @since 20220314
 * Reference: https://learntodroid.com/how-to-send-json-data-in-a-post-request-in-android/
 */

import com.example.myapplicationbasicfinal12345.api.ApiWatcher;
import com.example.myapplicationbasicfinal12345.api.JsonInvoiceApi;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class InvoiceRepository {

    private static InvoiceRepository instance;

    private JsonInvoiceApi jsonInvoiceApi;

    public static InvoiceRepository getInstance() {
        if (instance == null) {
            instance = new InvoiceRepository();
        }
        return instance;
    }

    public InvoiceRepository() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiWatcher.API_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonInvoiceApi = retrofit.create(JsonInvoiceApi.class);
    }

    public JsonInvoiceApi getInvoiceService() {
        return jsonInvoiceApi;
    }

}
