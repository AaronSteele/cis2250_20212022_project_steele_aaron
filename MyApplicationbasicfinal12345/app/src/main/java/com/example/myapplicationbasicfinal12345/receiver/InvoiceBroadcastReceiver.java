package com.example.myapplicationbasicfinal12345.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class InvoiceBroadcastReceiver extends BroadcastReceiver {
    public InvoiceBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(context, "Action: " + intent.getAction(), Toast.LENGTH_SHORT).show();
        Log.d("BJM receiver","Order Created...Action: " + intent.getAction());
    }
}