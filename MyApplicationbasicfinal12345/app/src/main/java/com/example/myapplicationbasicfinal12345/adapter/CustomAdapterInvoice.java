package com.example.myapplicationbasicfinal12345.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationbasicfinal12345.R;
import com.example.myapplicationbasicfinal12345.entity.Invoice;

import java.util.List;
//import info.hccis.performancehall_mobileappbasicactivity.R;
//import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;

public class CustomAdapterInvoice extends RecyclerView.Adapter<CustomAdapterInvoice.InvoiceViewHolder> {

    private List<Invoice> invoiceArrayList;

    public CustomAdapterInvoice(List<Invoice> invoiceBOArrayList) {
        this.invoiceArrayList = invoiceBOArrayList;
    }


    @NonNull
    @Override
    public InvoiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_invoice, parent, false);
        return new InvoiceViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull InvoiceViewHolder holder, int position) {

        String invoiceID = "" + invoiceArrayList.get(position).getId();
        String tutorID = "" + invoiceArrayList.get(position).getTutorId();
        String startDate = "" + invoiceArrayList.get(position).getStartDate();
        String endDate = "" + invoiceArrayList.get(position).getEndDate();
        holder.textViewInvoiceID.setText(invoiceID);
        holder.textViewTutorID.setText(tutorID);
        holder.textViewStartDate.setText(startDate);
        holder.textViewEndDate.setText(endDate);

    }

    @Override
    public int getItemCount() {
        return invoiceArrayList.size();
    }

    public class InvoiceViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewInvoiceID;
        private TextView textViewTutorID;
        private TextView textViewStartDate;
        private TextView textViewEndDate;

        public InvoiceViewHolder(final View view) {
            super(view);
            textViewInvoiceID = view.findViewById(R.id.textViewInvoice_ID_test);
            textViewTutorID = view.findViewById(R.id.textViewTutor_ID_test);
            textViewStartDate = view.findViewById(R.id.textViewStart_date_test);
            textViewEndDate = view.findViewById(R.id.textViewEnd_date_test);
        }
    }


}
