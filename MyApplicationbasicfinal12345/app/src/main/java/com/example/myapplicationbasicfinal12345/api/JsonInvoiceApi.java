package com.example.myapplicationbasicfinal12345.api;

import com.example.myapplicationbasicfinal12345.entity.Invoice;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonInvoiceApi {

    /**
     * This abstract method to be created to allow retrofit to get list of ticket orders
     * @return List of ticket orders
     * @since 20220202
     * @author BJM (with help from the retrofit research!).
     */

    @GET("invoice")
    Call<List<Invoice>> getInvoices();
    @POST("invoice")
    Call<Invoice> createInvoice(@Body Invoice invoice);

}