package com.example.myapplicationbasicfinal12345.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

/**
 * Class brought from the Performance Hall web application.  It also has some additional methods
 * related to Ticket Orders.  It's attributes correspond to the attributes which are found in
 * the database.
 *
 * @author bjmaclean
 * @since 20220202
 */
@Entity(tableName = "Invoice")
public class Invoice implements Serializable {

    //Note these match the field names from the database (and keys in json)
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int tutorId;
    private String startDate;
    private String endDate;


    public Invoice() {
    }

    public Invoice(Integer invoiceIDEntity) {
        this.id = invoiceIDEntity;
    }

    public int getId() {
        return id;
    }

    public void setId(int invoiceIDEntity) {
        this.id = invoiceIDEntity;
    }


    public int getTutorId() {
        return
                tutorId;
    }

    public void setTutorId(int tutorId) {
        this.tutorId = tutorId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

/**
     * Calculate the cost for a ticket order
     *
     * @return cost
     * @since 20220105
     * @author cis2250
     */
//    public double calculateTicketPrice() {
//        double discount = 0;
//        if(numberOfTickets >=20){
//            discount += DISCOUNT_VOLUME_20;
//        }else if(numberOfTickets >=10){
//            discount += DISCOUNT_VOLUME_10;
//        }
//
//        if(validateHollPassNumber()){
//            discount += DISCOUNT_HOLLPASS;
//        }
//
//        costOfTickets = numberOfTickets * COST_TICKET * (1-discount);
//
//        return costOfTickets;
//    }

    /**
     * Verify if a HollPassNumber if correct
     * Rules:  Length is 5 and a multiple of 13
     *
     * @return valid result
     * @since 20220105
     * @author cis2250
     */
//    public boolean validateHollPassNumber() {
//
//        boolean valid = false;
//
//        if(hollpassNumber >= 10000 && hollpassNumber < 100000){
//            if(hollpassNumber % 13 == 0){
//                valid=true;
//            }
//        }
//
//        return valid;
//    }

    /**
     * Return if a valid number of tickets or not
     * @return true if valid
     * @since 20220117
     * @author BJM
     */
//    public boolean validateNumberOfTickets(){
//        if(numberOfTickets > 0 && numberOfTickets <= MAX_TICKETS){
//            return true;
//        }else{
//            return false;
//        }
//    }

    @Override
    public String toString() {
        return "Invoice" + System.lineSeparator()
                + "Invoice ID=" + id +System.lineSeparator()
                + "Tutor ID=" + tutorId + System.lineSeparator()
                + "Start Date" + startDate + System.lineSeparator()
                + "End Date" + endDate + System.lineSeparator();
    }
}
