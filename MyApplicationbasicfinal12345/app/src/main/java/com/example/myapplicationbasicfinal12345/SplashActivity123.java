package com.example.myapplicationbasicfinal12345;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity123 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Sleeping for a bit to let user usee the splash screen.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        startActivity(new Intent(SplashActivity123.this, GoogleSignInActivity.class));
        startActivity(new Intent(SplashActivity123.this, MainActivity.class));
        finish();
    }
}

