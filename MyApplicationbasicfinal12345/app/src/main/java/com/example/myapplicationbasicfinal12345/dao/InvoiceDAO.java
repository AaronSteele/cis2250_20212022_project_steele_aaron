package com.example.myapplicationbasicfinal12345.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.myapplicationbasicfinal12345.entity.Invoice;

import java.util.List;

@Dao
public interface InvoiceDAO {

    @Insert
    void insert(Invoice invoice);

    @Query("SELECT * FROM Invoice")
    List<Invoice> selectAllInvoices();

    @Query("delete from Invoice")
    public void deleteAll();

}
