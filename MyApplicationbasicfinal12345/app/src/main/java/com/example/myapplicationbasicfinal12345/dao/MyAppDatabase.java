package com.example.myapplicationbasicfinal12345.dao;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.myapplicationbasicfinal12345.entity.Invoice;

@Database(entities = {Invoice.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract InvoiceDAO invoiceDAO();
}

