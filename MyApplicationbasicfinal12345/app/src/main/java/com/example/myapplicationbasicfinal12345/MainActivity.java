package com.example.myapplicationbasicfinal12345;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import com.example.myapplicationbasicfinal12345.api.ApiWatcher;
import com.example.myapplicationbasicfinal12345.dao.MyAppDatabase;
import com.example.myapplicationbasicfinal12345.databinding.ActivityMainBinding;
import com.example.myapplicationbasicfinal12345.receiver.InvoiceBroadcastReceiver;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;
    private static ApiWatcher apiWatcher;
    private InvoiceBroadcastReceiver invoiceBroadcastReceiver = new InvoiceBroadcastReceiver();

    private static MyAppDatabase myAppDatabase;

    public static MyAppDatabase getMyAppDatabase() {
        return myAppDatabase;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

//        binding.fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        apiWatcher = new ApiWatcher();
        apiWatcher.setActivity(this);
        apiWatcher.start();  //Start the background thread

        //****************************************************************************************
        //Set the database attribute
        //****************************************************************************************
        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "tutorInvoicedb").allowMainThreadQueries().build();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.example.myapp.invoice");
//        registerReceiver(exampleReceiver, filter);
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        lbm.registerReceiver(invoiceBroadcastReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        Log.d("BJM receiver", "unregistering");
        lbm.unregisterReceiver(invoiceBroadcastReceiver);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.d("MainActivity BJM Settings", "Option selected Settings");
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_about) {
            Log.d("MainActivity BJM About", "Option selected About");
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_googleLogin) {
            Log.d("MainActivity BJM Google login", "Option selected About");
            Intent intent = new Intent(this, GoogleSignInActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }
}