package com.example.myapplicationbasicfinal12345;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplicationbasicfinal12345.adapter.CustomAdapterInvoice;
import com.example.myapplicationbasicfinal12345.databinding.FragmentViewInvoicesBinding;
import com.example.myapplicationbasicfinal12345.entity.Invoice;
import com.example.myapplicationbasicfinal12345.entity.InvoiceViewModel;
import com.example.myapplicationbasicfinal12345.util.NotificationApplication;
import com.example.myapplicationbasicfinal12345.util.NotificationUtil;

import java.util.List;




public class ViewInvoicesFragment extends Fragment {

    private static Context context;
    private FragmentViewInvoicesBinding binding;
    private List<Invoice> invoiceArrayList;
    private static RecyclerView recyclerView;
    public static RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public static void notifyDataChanged(String message){
        Log.d("bjm", "Data changed:  "+message);
        //Send a notification that the data has changed.
        try {
            recyclerView.getAdapter().notifyDataSetChanged();
        }catch(Exception e){
            Log.d("bjm api","Exception when trying to notify that the data set as changed");
        }
    }

    /**
     * Provide notification tha the data has changed.  This method will notify the adapter that the
     * rows have changed so it will know to refresh.  It will also send a notification to the user which
     * will allow them to go directly back to the list from another activity of the app.
     * @param message Message to display
     * @param activity - originating activity
     * @param destinationClass - class associated with the intent associated with the notification.
     */
    public static void notifyDataChanged(String message, Activity activity, Class destinationClass){
        Log.d("bjm", "Data changed:  "+message);
        try {
            notifyDataChanged(message);
            NotificationApplication.setContext(context);
            NotificationUtil.sendNotification("PHall Data Update", message, activity, MainActivity.class);
        }catch(Exception e){
            Log.d("bjm notification", "Exception occured when notifying. "+e.getMessage());
        }



    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentViewInvoicesBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //************************************************************************************
        // Corresponding to the add fragment, the view model is accessed to obtain a reference
        // to the list of invoice bo objects.
        //************************************************************************************
        InvoiceViewModel invoiceViewModel = new ViewModelProvider(getActivity()).get(InvoiceViewModel.class);

        //************************************************************************************
        //NO LONGER USED BUT LEFT AS EXAMPLE
        //Bundle is accessed to get the invoice which is passed from the add invoice fragment.
        //************************************************************************************
        //***Aaron: I'm not editing the commented example code shown below.***
        //        Bundle bundle = getArguments(); //Note not doing anything...here for example
        //        TicketOrder ticketOrder = (TicketOrder) bundle.getSerializable(AddOrderFragment.KEY);
        //        Log.d("ViewOrdersFragment BJM", "Ticket passed in:  " + ticketOrder.toString());

        //************************************************************************************
        //Build the output to be displayed in the textview
        // NOTE:  This output string is no longer displayed since we added the RecyclerView
        //************************************************************************************


        //************************************************************************************
        // Set the context to be used when sending notifications
        //************************************************************************************

        context = getView().getContext();

        //************************************************************************************
        // Setup the recycler view for displaying the items in the invoice list.
        //************************************************************************************

        recyclerView = binding.recyclerView;

        invoiceArrayList = invoiceViewModel.getInvoices();
        setAdapter();

        //************************************************************************************
        // Button sends the user back to the add fragment
        //************************************************************************************

        binding.buttonAddInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewInvoicesFragment.this)
                        .navigate(R.id.action_ViewInvoicesFragment_to_AddInvoiceFragment);
            }
        });
        binding.buttonViewCarousel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewInvoicesFragment.this)
                        .navigate(R.id.action_ViewInvoicesFragment_to_CarouselFragment);
            }
        });
    }

    /**
     * Set the adapter for the recyclerview
     * @since 20220129
     * @author BJM
     */
    private void setAdapter() {
        CustomAdapterInvoice adapter = new CustomAdapterInvoice(invoiceArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}