package com.example.myapplicationbasicfinal12345.entity;

import android.util.Log;

import com.example.myapplicationbasicfinal12345.MainActivity;

import java.util.List;

public class InvoiceContent {

    /**
     * This method will take the list passed in and reload the room database
     * based on the items in the list.
     * @param invoices
     * @since 20220210
     * @author BJM
     */
    public static void reloadInvoicesInRoom(List<Invoice> invoices)
    {
        MainActivity.getMyAppDatabase().invoiceDAO().deleteAll();
        for(Invoice current : invoices)
        {
            MainActivity.getMyAppDatabase().invoiceDAO().insert(current);
        }
        Log.d("BJM Room","loading ticket orders into Room");
    }


    /**
     * This method will obtain all the ticket orders out of the Room database.
     * @since 20220210
     * @author BJM
     */
    public static void getInvoicesFromRoom()
    {
        Log.d("BJM Room","Loading ticket orders from Room");

        List<Invoice> invoicesBack = MainActivity.getMyAppDatabase().invoiceDAO().selectAllInvoices();
        Log.d("BJM Room","Number of ticket orders loaded from Room: " + invoicesBack.size());
        for(Invoice current : invoicesBack)
        {
            Log.d("BJM Room",current.toString());
        }
    }

}